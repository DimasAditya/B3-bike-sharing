from django.shortcuts import render
from .models import models_voucher
from .forms import form_voucher
import json

# Create your views here.
def voucher(request):
	response = {'form' : form_voucher}
	response['formcher'] = form_voucher
	return render(request, 'voucher.html', response)

def post_voucher(request):
	form = voucher(request.POST or None)
	if (form.is_valid()):
		datas = form.cleaned_data
		datas = models_voucher(nama=datas['nama'], kategori=datas['kategori'], nilai_poin=datas['nilai_poin'], deskripsi=datas['deskripsi'], jumlah=datas['jumlah'])
		datas.save()
		data = {'nama': datas['name'], 'kategori': datas['kategori'], 'nilai_poin': datas['nilai_poin'], 'deskripsi': datas['deskripsi'], 'jumlah': datas['jumlah']}
		response['models_voucher'] = request.POST['models_voucher']
		listvoucher = models_voucher(StatusHari=response['models_voucher'])
		listvoucher.save()
		response['listvoucher'] = models_voucher.objects.all()
		return JsonResponse(data)

def peminjaman(request):
	return render(request, 'peminjaman.html')