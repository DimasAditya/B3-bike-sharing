from django.urls import re_path, path
from .views import voucher, post_voucher, peminjaman

urlpatterns = [
	path('voucher', voucher, name='voucher'),
	path('post_voucher', post_voucher, name='post_voucher'),
	path('pinjam', peminjaman, name='peminjaman'),

]
