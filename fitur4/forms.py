from django import forms

# Create your models here.
class form_voucher(forms.Form):
    nama = forms.CharField(widget=forms.TextInput(), max_length=255)
    kategori = forms.CharField(widget=forms.TextInput(), max_length=255);
    nilai_poin = forms.FloatField(widget=forms.NumberInput());
    deskripsi = forms.CharField(widget=forms.TextInput());
    jumlah = forms.IntegerField(widget=forms.NumberInput());
