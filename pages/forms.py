from django import forms
from django.forms.widgets import TextInput, DateTimeInput

from .models import JadwalPribadi

class JadwalPribadiForm(forms.ModelForm):

	class Meta:
		model = JadwalPribadi
		fields = ('event', 'waktu', 'tempat', 'kategori')
		widgets = {
			'event': TextInput(attrs={'class': 'form-control', 'placeholder': 'Nama Event'}),
			'tempat': TextInput(attrs={'class': 'form-control', 'placeholder': 'Tempat Event'}),
			'waktu': DateTimeInput(attrs={'class': 'form-control', 'placeholder': 'yyyy-mm-dd HH:MM:SS', 'type': 'datetime-local'}),
			'kategori': TextInput(attrs={'class': 'form-control', 'placeholder': 'Kategori Event'}),
		}