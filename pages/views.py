from django.shortcuts import render, redirect
from .models import JadwalPribadi, RegisteredUser
from .forms import JadwalPribadiForm
from django.http import JsonResponse
from django.core import serializers
import requests
import json

# Create your views here.
def homePageView(request):
    return render(request, 'main.html')


def formSaranView(request):
    return render(request, 'formsaran.html')


def registerView(request):
    return render(request, 'register.html')

def loginView(request):
    return render(request, 'login.html')

def transaksiView(request):
    return render(request, 'transaksi.html')

def riwayatTransaksiView(request):
    return render(request, 'riwayatTransaksi.html')

def daftarLaporanView(request):
    return render(request, 'daftarLaporan.html')



def postJadwal(request):
    if request.method == "POST":
        form = JadwalPribadiForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('jadwalkegiatan')
        else:
            return redirect('jadwalkegiatan')
    else:
        form = JadwalPribadiForm()
        data = JadwalPribadi.objects.all()
        return render(request, "jadwalkegiatan.html", {'form': form, 'data': data})




def validateUser(request):
    email = request.GET.get("email")
    r = {}
    
    r["email_exist"] = RegisteredUser.objects.filter(email=email).exists()

    return JsonResponse(r)

def registerUser(request):
    email = request.POST.get("email")
    password = request.POST.get("password")
    name = request.POST.get("name")
    birthdate = request.POST.get("birthdate")

    register_success = not RegisteredUser.objects.filter(email=email).exists()

    if (register_success):
        user = RegisteredUser(email=email, password=password, name=name, birthdate=birthdate)
        user.save()

    r = {"register_success": register_success}

    return JsonResponse(r)

def getUser(request):
    r = {}
    r["user_registered"] = RegisteredUser.objects.all().count()
    data = serializers.serialize('json', RegisteredUser.objects.all())
    r["users"] = json.loads(data)
    return JsonResponse(r)


def deleteUser(request):
    password = request.POST.get("password")
    pk = request.POST.get("pk")
    user = RegisteredUser.objects.all().get(pk=pk)
    r = {}
    r["users"] = json.loads(serializers.serialize('json', [user,]))
    if (password == user.password):
        user.delete()
        r["user_deleted"] = True
    else:
        r["user_deleted"] = False

    return JsonResponse(r)

def saveToSession(request):
    if 'favorite_book' not in request.session:
        request.session['favorite_book'] = 0
    else:
        is_delete = True if request.GET.get('is_delete') == 'true' else False
        if is_delete:
            request.session['favorite_book'] -= 1
        else:
            print('ditambah')
            request.session['favorite_book'] += 1
    
    r = {}
    r["favorite_book"] = request.session['favorite_book']
    return JsonResponse(r)