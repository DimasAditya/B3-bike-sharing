from django.urls import path
from django.contrib.auth.views import LogoutView
from django.conf import settings
from .views import (homePageView, formSaranView, registerView,
                    postJadwal, registerUser, validateUser,
                    getUser, deleteUser, saveToSession, transaksiView, riwayatTransaksiView,daftarLaporanView, loginView)

urlpatterns = [
    path('formsaran/', formSaranView, name='formsaran'),
    path('register/', registerView, name='register'),
    path('register/register/', registerUser, name='register_user'),
    path('register/validate/', validateUser, name='validate_user'),
    path('register/getuser/', getUser, name='get_user'),
    path('register/deleteuser/', deleteUser, name='delete_user'),

    path('transaksi/', transaksiView, name='transaksi'),
    path('transaksi/riwayat', riwayatTransaksiView, name='riwayat_transaksi'),
    path('laporan/daftarlaporan', daftarLaporanView, name='daftar_laporan'),
    path('login', loginView, name='login'),


    path('', homePageView, name='home'),
    path('auth/logout/', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    path('savetosession/', saveToSession, name='save_to_session')
]