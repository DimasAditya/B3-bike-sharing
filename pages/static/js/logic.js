function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function deleteUser(pk) {
    var password = window.prompt("Enter your password");
    $.ajax({
        type: "POST",
        url: "/register/deleteuser/",
        data: {"pk": pk, "password": password},
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        success: function(data) {
            if (data.user_deleted) {
                alert("User successfully deleted");
                $("#show-user").trigger("click");
            } else {
                alert("Password not match!");
            }
        }
    })
}

$(document).ready(function() {
    var validateSuccess;

    $("#show-user").click(function() {
        $.ajax({
            url: "/register/getuser/",
            success: function(data) {
                $("#user-container").hide();
                var html = "";
                if (data.users.length == 0) {
                    html += "<h1>No User Registered</h1>"
                    $("#user-container").html(html);
                    $("#user-container").fadeIn("normal");
                    return;
                }
                html ="<table class='table table-bordered'><tr><th>Email</th><th>Name</th><th>Birth Date</th><th>Unsubscribe</th></tr>";
                for(var i = 0; i < data.users.length; i++) {
                    html += "<tr>"
                    html += "<td>" + data.users[i].fields.email + "</td>";
                    html += "<td>" + data.users[i].fields.name + "</td>";
                    html += "<td>" + data.users[i].fields.birthdate + "</td>";
                    html += "<td><button class='btn btn-danger' onclick='deleteUser(" + data.users[i].pk + ")'>Unsubscribe</button></td>";
                    html += "</tr>"
                }
                html += "</table>"
                $("#user-container").html(html);
                $("#user-container").fadeIn("normal");
            }
        })
    })

    $("#show-user").trigger("click");

    $("#show-book").click(function() {
        $.ajax({
            url: "/generatebuku/",
            data: {"search": $("#search").val()},
            beforeSend: function() {
                $("#table-container").html("processing");
            },
            success: function(data) {
                $("#counter").text(data.favorite_book);
                try {
                    data.items.length;
                } catch (TypeError) {
                    $("#table-container").html("not found");
                    return;
                }
                var volumeInfo;
                var html;
                for (var i = 0; i < data.items.length; i++) {
                    volumeInfo = data.items[i].volumeInfo;
                    html += "<tr>";
                    html += "<td class='align-middle text-center'>" + (i + 1) + "</td>";
                    html += "<td class='align-middle text-center'>";
                    try {
                        volumeInfo.imageLinks.thumbnail;
                        html += "<img src='";
                        html += volumeInfo.imageLinks.thumbnail;
                        html += "'>";
                    } catch (TypeError) {
                        html += "-";
                    }
                    html += "</td>";
                    html += "<td class='align-middle text-center'>" + volumeInfo.title + "</td>";
                    html += "<td class='align-middle text-center'>";
                    try {
                        for (var j = 0; j < volumeInfo.authors.length; j++) {
                            html += volumeInfo.authors[j];
                        }
                    } catch (TypeError) {
                        html += "-";
                    }
                    html += "</td>";
                    html +="<td class='align-middle text-center'><p class='fa fa-star'></p></td>";
                    html += "</tr>";
                }
                $("#table-container").html("<table class='table table-bordered'><thead><tr><th>Nomor</th><th>Gambar</th><th>Judul</th>" +
                    "<th>Penulis</th><th>Favorite</th></tr></thead><tbody id='output'></tbody></table>")
                $("#output").append(html);
                $(".fa-star").click(function() {
                    var ini = $(this);
                    if ($(this).hasClass("checked")) {
                        $.ajax({
                            url: '/savetosession/',
                            data: {is_delete: true},
                            success: function(data) {
                                ini.removeClass("checked");
                                var newVal = parseInt(data.favorite_book)
                                $("#counter").text(newVal)
                            }
                        })
                    } else {
                        $.ajax({
                            url: '/savetosession/',
                            data: {is_delete: false},
                            success: function(data) {
                                ini.addClass("checked");
                                var newVal = parseInt(data.favorite_book)
                                $("#counter").text(newVal)
                            }
                        })
                    }
                })
            }
        })
    })

    $("#email").on("input", function() {
        if (validateSuccess) {
            $("#submit-register").addClass("disabled");
            $("#submit-register").attr("disabled", true);
            $("#message-container").html("<div class='alert alert-info'>Data Changed. Validate Again!</div>");
            validateSuccess = false;
        }
    })

    $("#validate-button").on("click", function() {
        $("form").validate({
            rules: {
                password: {
                    minlength: 8,
                    equalTo: "#passwordver"
                },
                passwordver: {
                    minlength: 8,
                }
            }
        })
        if ($("form").valid()) {
            $.ajax({
                url: "/register/validate/",
                data: {email: $("#email").val()},
                success: function(data) {
                    $("#message-container").hide();
                    var html = "";
                    var validationSuccess = true;
                    if (data.email_exist) {
                        html += "<div class='alert alert-danger'>Email Registered</div>";
                        validationSuccess = false;
                    } else {
                        html += "<div class='alert alert-success'>Email Validated</div>";
                    }
    
                    if (validationSuccess) {
                        validateSuccess = true;
                        $("#submit-register").removeClass("disabled");
                        $("#submit-register").attr("disabled", false);
                    } else {
                        $("#submit-register").addClass("disabled");
                        $("#submit-register").attr("disabled", true);
                    }
    
                    $("#message-container").html(html);
                    $("#message-container").fadeIn("normal");
                }
            })
        }
    })

    $("form").submit(function(e) {
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/register/register/",
            data: data,
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            success: function(data) {
                $("#message-container").hide();
                if (data.register_success) {
                    var html = "<div class='alert alert-success'>Successfully Registered</div>";
                } else {
                    var html = "<div class='alert alert-danger'>Register Failed</div>";
                }
                $("#message-container").html(html);
                $("#message-container").fadeIn("slow");
                $("form").trigger("reset");
                $("#show-user").trigger("click");
            }
        })
    })
})