from django.db import models

# Create your models here.
class JadwalPribadi(models.Model):
	event = models.CharField(max_length=30)
	waktu = models.DateTimeField()
	tempat = models.CharField(max_length=30)
	kategori = models.CharField(max_length=30)
	
	class Meta:
		verbose_name_plural = 'Jadwal Pribadi(S)'
		verbose_name = 'Jadwal Pribadi'


class RegisteredUser(models.Model):
	email = models.EmailField(max_length=255)
	password = models.CharField(max_length=255)
	name = models.CharField(max_length=255)
	birthdate = models.DateField()

	class Meta:
		verbose_name = 'Registered User'
		verbose_name_plural = "Registered Users"

	def __str__(self):
		return self.email