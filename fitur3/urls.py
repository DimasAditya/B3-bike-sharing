from django.urls import path
from .views import sepedaView, stasiunView

urlpatterns=[
    path('sepeda/', sepedaView, name='sepeda'),
    path('stasiun/', stasiunView, name='stasiun'),
    
]