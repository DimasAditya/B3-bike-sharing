from django.shortcuts import render

# Create your views here.
def sepedaView(request):
    return render(request, 'sepeda.html')

def stasiunView(request):
    return render(request, 'stasiun.html')