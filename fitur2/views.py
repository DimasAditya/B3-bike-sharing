from django.shortcuts import render

# Create your views here.

def penugasanView(request):
    return render(request, 'penugasan.html')

def daftarPenugasanView(request):
    return render(request, 'daftarPenugasan.html')

def acaraView(request):
    return render(request, 'acara.html')

def daftarAcaraView(request):
    return render(request, 'daftarAcara.html')
