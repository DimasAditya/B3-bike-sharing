from django.urls import path
from .views import(penugasanView, daftarPenugasanView, acaraView, daftarAcaraView)

urlpatterns=[
    path('penugasan/', penugasanView, name='penugasan'),
    path('acara/', acaraView, name='acara'),
    path('daftarAcara/', daftarAcaraView, name='daftarAcara'),
    path('daftarPenugasan/', daftarPenugasanView, name='daftarPenugasan')
]